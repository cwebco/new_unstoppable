<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session, file;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
//Model
use App\Model\Setting;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *s
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $setting = Setting::first();        
        return view('setting.setting')->with('setting',$setting);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $rules =  array(
            'twillo_api' => 'required',
            'auth_token' => 'required',
            'twilio_number' => 'required',
            'sms_format' => 'required',
            'campaign_deactivated' => 'required',
            'wrong_keyword_sms' => 'required',
            'short_url' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
           return back()->withErrors($validator)->withInput();
        }
        
        $row = Setting::first();
        
        if($row){
            $setting = Setting::find($row->id);
        }else{
            $setting = new Setting;    
        }
        
        $setting->twillo_api = $request->twillo_api;
        $setting->auth_token = $request->auth_token;
        $setting->twilio_number = $request->twilio_number;
        $setting->sms_format = $request->sms_format;
        $setting->campaign_deactivated = $request->campaign_deactivated;
        $setting->wrong_keyword_sms = $request->wrong_keyword_sms;
        $setting->short_url = $request->short_url;

        $status = $setting->save();
        if($status){
            
            $message ="Setting updated successfully";
            Session::flash('message', $message); 
            Session::flash('alert-class', 'alert-info'); 

            return redirect('setting');
        }else{

            $message ="There is an error , try again";

            Session::flash('message', $message); 
            Session::flash('alert-class', 'alert-danger');
            
            return redirect('setting');
        }        
    }   
}
