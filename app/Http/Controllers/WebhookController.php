<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Model\Campaign;
use Twilio\Rest\Client;
use App\Model\Setting;
use App\Model\Campaignstat;

class WebhookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function short_url(Request $request){
        // Redirect browser 
        $raw_url = url('/').'/short_url/'.$request->url;
        $url = Campaign::where('short_link',$raw_url)->first();
        $proudct_page_link =  $url->proudct_page_link;
        header("Location: $proudct_page_link");   
        exit; 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function sms(Request $request)
    {
        if(@$request->MessageSid){
            Log::info($request->From);          
            Log::info($request->Body);

            $setting = Setting::first();
            
            $sid    = $setting->twillo_api;
            $token  = $setting->auth_token;
            $twilio_number  = $setting->twilio_number;

            $twilio = new Client($sid, $token);
            $phone = $request->From;
            $keyword = $request->Body;

            $campaign = Campaign::where('keyword',$keyword)->first();
            $campaignstat = new Campaignstat;
            $campaignstat->phone_number = $phone;
            $campaignstat->keyword = $keyword;
           
            if($campaign){
                $campaignstat->campaign_id = $campaign->id;

                if($campaign->is_active==1){
                    $message = $campaign->sms_content!="" ? $campaign->sms_content : $setting->sms_format;
                    $message = str_replace("{short_link}",$campaign->short_link,$message);
                    $message = str_replace("{keyword}",$campaign->keyword,$message);

                }else{
                    $message = $setting->campaign_deactivated;
                    $message = str_replace("{keyword}",$campaign->keyword,$message);
                }
            }else{
                $message = $setting->wrong_keyword_sms;
                $message = str_replace("{keyword}",$keyword,$message);
            }

            $campaignstat->sent_sms_content = $message;

            try{
                $message = $twilio->messages
                              ->create('+'.$phone, // to
                                       ["body" => $message, "from" => $twilio_number]
                              );

                $campaignstat->sms_deivery_status = 1;
            
            } catch (\Exception $e) {
                
                $campaignstat->sms_deivery_status = 0;
                $campaignstat->reason = $e->getMessage();

                Log::info($e->getMessage());
            }  

            $campaignstat->on_dt = date('Y-m-d H:i:s');
            $campaignstat->save();                

            if(@$message->sid){
                Log::info($message->sid."  message sent");                
            }else{
                Log::info("message not sent");                
            }          
        }
    }    
}
