<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session, file;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Twilio\Rest\Client as TwilioClient;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

//Model
use App\Model\Campaign;
use App\Model\Customer;
use App\Model\Campaignstat;
use Helper;

class CampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {          
        if(isset($request->search) && $request->search !=""){
            $campaign = Campaign::where('name',$request->name)->paginate(10);
        }else{
            $campaign = Campaign::orderBy('created_dt', 'desc')->paginate(10);
        } 
           
        return view('campaign.list')->with('campaign', $campaign);
    }

    public function convertIntToShortCode() {
        $digits = 6;
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =  array(
            'name' => 'required',
            'product_pages_url' => 'required|url',
            'author_name' => 'required',
            'book_title' => 'required',
            'text_keyword' => 'required|unique:campaign,keyword',
            'start_date' => 'required',
            //'sms_format_text' => 'required',
            //'short_url' => 'required'
        );

        $validator = Validator::make($request->all(), $rules,[
            'name.required' => 'A name is required',
            'product_pages_url.required' => 'A product pages url is required',
            'author_name.required' => 'A author name is required',
            'book_title.required' => 'A book title is required',
            'text_keyword.required' => 'A text keyword is required',
            'text_keyword.unique' => 'A text keyword already taken',
            'start_date.required' => 'A start date is required',
            'sms_format_text.required' => 'A sms format text is required',
        ]);

        if ($validator->fails())
        {
           return back()->withErrors($validator)->withInput();
        }

        $campaign = new Campaign;
        $campaign->name = $request->name;
        $campaign->proudct_page_link = $request->product_pages_url;
        $campaign->author_name = $request->author_name;
        $campaign->book_title = $request->book_title;
		$campaign->keyword = $request->text_keyword;
        $campaign->short_link = URL('/').'/short_url/'.$this->convertIntToShortCode();
        $campaign->is_active = $request->status;
        $campaign->start_date = date('Y-m-d', strtotime($request->start_date));
        $campaign->sms_content = $request->sms_format_text;
        $campaign->created_ip = $_SERVER['REMOTE_ADDR'];
        $campaign->created_dt = date('Y-m-d H:i:s');

        $status = $campaign->save();
        if($status){
            $message ="Campaign created successfully";            
            Session::flash('message', $message); 
            Session::flash('alert-class', 'alert-info'); 
            
            return redirect('dashboard');
        }else{
            $message ="There is an error , try again";

            Session::flash('message', $message); 
            Session::flash('alert-class', 'alert-danger'); 

            return redirect('dashboard');
        }        
    }

    public function edit_save(Request $request, $id,$page=1)
    {
        $rules =  array(
            'name' => 'required',
            'product_pages_url' => 'required|url',
            'author_name' => 'required',
            'book_title' => 'required',
            'text_keyword' => 'required|unique:campaign,keyword,'.$id,
            'start_date' => 'required',
            //'sms_format_text' => 'required',
            //'short_url' => 'required'
        );

        $validator = Validator::make($request->all(), $rules, [
            'name.required' => 'A name is required',
            'product_pages_url.required' => 'A product pages url is required',
            'author_name.required' => 'A author name is required',
            'book_title.required' => 'A book title is required',
            'text_keyword.required' => 'A text keyword is required',
            'text_keyword.unique' => 'A text keyword already taken',
            'start_date.required' => 'A start date is required',
            'sms_format_text.required' => 'A sms format text is required',
        ]);

        if ($validator->fails())
        {
           return back()->withErrors($validator)->withInput();
        }
        
        $campaign = Campaign::find($id);;
        $campaign->name = $request->name;
        $campaign->proudct_page_link = $request->product_pages_url;
        $campaign->author_name = $request->author_name;
        $campaign->book_title = $request->book_title;
        $campaign->keyword = $request->text_keyword;
        //$campaign->short_link = $request->short_url;
        $campaign->is_active = $request->status;
        $campaign->start_date = date('Y-m-d', strtotime($request->start_date));
        $campaign->sms_content = $request->sms_format_text;

        $status = $campaign->save();
        if($status){
            
            $message ="Campaign updated successfully";
            
            Session::flash('message', $message); 
            Session::flash('alert-class', 'alert-info'); 
            
            return redirect("dashboard?page=$page");
        }else{
            $message ="There is an error , try again";

            Session::flash('message', $message); 
            Session::flash('alert-class', 'alert-danger'); 

           return redirect("dashboard?page=$page");
        }

        //echo "<script> history.go(-2); </script>"; 
    }

    //For Show Add or edit Page
    public function add()
    {
        return view('campaign.add');
    }

    public function edit(Request $request, $id, $page=1){
        $campaign = Campaign::find($id);
        return view('campaign.edit')->with('campaign',$campaign)->with('page',$page);
    }  

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MetaData  $metaData
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);
        @$campaign->delete();

        $message = 'Campaign deleted successfully!';

        Session::flash('message', $message); 
        Session::flash('alert-class', 'alert-info');
        return back();
    }

    public function customer_delete($id)
    {
        $client = Customer::find($id);
        @$client->delete();
        return back()->with('success','Data deleted successfully!');
    }

    public function static($id, $from=4)
    {

        $total = 0;
        if($from==4){
            $total = Campaignstat::where('campaign_id',$id)->get();
            $total = count($total);
        }

        if($from==1){
            $currentMonth = date('m');
            $total = Campaignstat::whereRaw('MONTH(on_dt) = ?',[$currentMonth])->where('campaign_id',$id)->get();
            $total = count($total);
        }

        if($from==2){
            $total = Campaignstat::where('on_dt', '>', Carbon::now()->startOfWeek())
     ->where('on_dt', '<', Carbon::now()->endOfWeek())->where('campaign_id',$id)->get();
            $total = count($total);
        }

        if($from==3){
            $total = Campaignstat::whereDate('on_dt', Carbon::today())->where('campaign_id',$id)->get();
            $total = count($total);
        }

        return view('campaign.static')->with('total',$total)->with('id',$id)->with('from',$from);
    }
}
