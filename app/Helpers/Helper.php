<?php
namespace App\Helpers;
class Helper{

    public static function send_sms( $key, $mask, $recipient, $message,$callback_url)
    {
        $input_data = array('recipient'=>$recipient,'content'=>$message,'mask'=>$mask,'callback'=>$callback_url);

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://eziapi.com/v3/sms",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($input_data),
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "key: $key",
          ),
        ));

        $response = curl_exec($curl);
        
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          $res = json_decode($response, true);  
          return $res;
        }
    }

    public static function create_contact( $key, $first_name, $last_name, $mobile) {

        $input_data = array('first_name'=>$first_name,'last_name'=>$last_name,'mobile'=>$mobile);

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://eziapi.com/v3/contacts",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($input_data),
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "key: $key",
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          return "error";
        } else {
          $res = json_decode($response, true);  
          return $res;
        }
    }

    public static function update_contact( $key, $first_name, $last_name, $mobile, $id) {

        $input_data = array('first_name'=>$first_name,'last_name'=>$last_name,'mobile'=>$mobile);

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://eziapi.com/v3/contacts",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($input_data),
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "key: $key",
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          $res = json_decode($response, true);  
          return $res;
        }
    }
}

