<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'setting';
    public $timestamps = false;

    protected $fillable = [
        'twillo_api','sms_format','campaign_deactivated','wrong_keyword_sms','short_url', 'auth_token', 'twilio_number'
    ];
}
