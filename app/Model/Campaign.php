<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'campaign';
    public $timestamps = false;

    protected $fillable = [
        'name', 'author_name', 'keyword', 'proudct_page_link', 'short_link', 'created_dt','created_ip', 'start_date', 'sms_content', 'is_active', 'book_title'
    ];
}
