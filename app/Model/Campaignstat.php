<?php

namespace App\model;
use Illuminate\Database\Eloquent\Model;

class Campaignstat extends Model
{
    protected $table = 'campaign_stat';
    public $timestamps = false;

    protected $fillable = [
        'campaign_id', 'sent_sms_content', 'on_dt', 'sms_deivery_status', 'phone_number','keyword','reason'
    ];
}
