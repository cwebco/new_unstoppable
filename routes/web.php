<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/itstome', function () {
    echo date('Y-m-d H:i:s');
});


//Manage Clients or Resuturant
Route::get('/dashboard','CampaignController@index');
Route::get('/campaign/add','CampaignController@add');
Route::post('/campaign/save','CampaignController@store');
Route::get('/campaign/edit/{id}/{page}','CampaignController@edit');
Route::get('/campaign/{id}','CampaignController@destroy');
Route::post('/campaign/update/{id}/{page}','CampaignController@edit_save');
Route::get('/static/{id}/{from}','CampaignController@static');

Route::get('/short_url/{url}','WebhookController@short_url');
Route::get('/sms','WebhookController@sms');
Route::get('/send_sms/{phone_no}/{keyword}','WebhookController@send_sms');

//Manage Settings
Route::get('/setting','SettingController@index');
Route::post('/setting/update','SettingController@edit');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');