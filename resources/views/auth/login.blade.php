@extends('layout.frontend')
@section('content')

<div class="kt-grid kt-grid--ver kt-grid--root">
            <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(./assets/media//bg/bg-3.jpg);">
                    <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                        <div class="kt-login__container">
                            <div class="kt-login__logo">
                                <a href="#">
                                    <img src="assets/media/logos/logo-5.png">
                                </a>
                            </div>

                            <div class="kt-login__signin">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Sign In To Unstoppable User Login Interface</h3>
                                </div>

                                @foreach ($errors->all() as $error)
                                    @if($error=='auth.failed')
                                    <li style="color: red;">Username or password does not exist.</li>
                                    @else
                                    @if($error!='passwords.user')
                                    <li style="color: red;">{{ $error }}</li>
                                    @endif
                                    @endif
                                @endforeach

                                @if (session('status'))
                                   @if(session('status')!='passwords.sent') 
                                   <p class="alert alert-success">{{ session('status') }}</p>
                                   @endif
                                @endif
                               

                                <form class="kt-form" action="login" method="post">
                                    @csrf
                                    <div class="input-group">
                                        <input class="form-control" type="email" placeholder="Email" name="email" autocomplete="off" required="">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="password" placeholder="Password" name="password" required="">
                                    </div>
                                    <div class="row kt-login__extra">
                                        <div class="col">
                                            <label class="kt-checkbox">
                                                <input type="checkbox" name="remember"> Remember me
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col kt-align-right forget_pass">
                                            <a href="" id="kt_login_forgot" class="kt-login__link">Forget Password ?</a>
                                        </div>
                                    </div>
                                    <div class="kt-login__actions">
                                        <!-- <button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary"> --><button type="submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button><!-- </button> -->
                                    </div>
                                </form>
                            </div>


                            <div class="kt-login__signup">
                                
                                 <a style="padding-top: 7px;display: none;"  class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <b class="btn btn-label btn-label-brand btn-sm btn-bold">{{ __('Logout') }}</b>
                                </a>        
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Sign Up</h3>
                                    <div class="kt-login__desc">Enter your details to create your account:</div>
                                </div>

                                @foreach ($errors->all() as $error)
                                        <li style="color: red;">{{ $error }}</li>
                                @endforeach
                               
                                <form class="kt-form" action="register" name="register" method="post">
                                    @csrf
                                    <div class="input-group">
                                        <input class="form-control" type="text" placeholder="Firstname" name="Firstname" required="" autocomplete="off">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="text" placeholder="Lastname" name="Lastname" required="" autocomplete="off">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="email" placeholder="Email" name="email" autocomplete="off" required="">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="password" placeholder="Password" name="password" required="" minlength="8" autocomplete="off">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="password" placeholder="Confirm Password" name="password_confirmation" minlength="8" required="" autocomplete="off">
                                    </div>
                                    <div class="row kt-login__extra">
                                        <div class="col kt-align-left">
                                            <label class="kt-checkbox">
                                                <input type="checkbox" name="agree" required="">I Agree the <a href="#" class="kt-link kt-login__link kt-font-bold">terms and conditions</a>.
                                                <span></span>
                                            </label>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="kt-login__actions">
                                        <button id="" class="btn btn-brand btn-elevate kt-login__btn-primary" name="register" type="submit">Sign Up</button>&nbsp;&nbsp;
                                        <button id="kt_login_signup_cancel" class="btn btn-light btn-elevate kt-login__btn-secondary">Cancel</button>
                                    </div>
                                </form>



                            </div>
                            <div class="kt-login__forgot">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Forgotten Password ?</h3>
                                    <div class="kt-login__desc">Enter your email to reset your password:</div>
                                </div>

                                @foreach ($errors->all() as $error)
                                        @if($error=='passwords.user')
                                        <li style="color: red;">Wrong email id enter</li>
                                        @endif
                                @endforeach

                                @if (session('status'))
<p class="alert alert-success">@if(session('status')=='passwords.sent') Reset link sent successfully  @endif</p>
                                @endif
                               
                               
                                <form name="forget_pass" class="kt-form" method="post" action="{{URL('/')}}/password/email">
                                    
                                    <div class="input-group">
                                        <input class="form-control" type="email" placeholder="Email" name="email" id="kt_email" autocomplete="off" required="">
                                    </div>
                                    @csrf
                                    <div class="kt-login__actions">
                                        <button type="submit" id="" class="btn btn-brand btn-elevate kt-login__btn-primary">Request</button>&nbsp;&nbsp;
                                        <button id="kt_login_forgot_cancel" class="btn btn-light btn-elevate kt-login__btn-secondary">Cancel</button>
                                    </div>
                                </form>
                            </div>
                            <div class="kt-login__account">
                                <span class="kt-login__account-msg">
                                    Don't have an account yet ?
                                </span>
                                &nbsp;&nbsp;
                                <a href="javascript:;" id="kt_login_signup" class="kt-login__account-link">Sign Up!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
